const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', (socket) => {
    socket.broadcast.emit('Bonjour !');
    socket.on('Chat message', (msg) => {
        console.log('Message : ' + msg);
        io.emit('chat message', msg);
    });
    console.log('User Tomo is connected');
    socket.on('disconnect', () => {
        console.log('User Tomo disconnected');
    });
});

http.listen(3003, () => {
    console.log('Listening on localhost:3003');
});